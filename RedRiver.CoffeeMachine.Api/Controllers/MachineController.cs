using Microsoft.AspNetCore.Mvc;
using RedRiver.CoffeeMachine.Api.Models;
using RedRiver.CoffeeMachine.Processor.Services.ChocolateServices;
using RedRiver.CoffeeMachine.Processor.Services.CoffeeServices;
using RedRiver.CoffeeMachine.Processor.Services.LemonTeaServices;

namespace RedRiverCoffeeMachineApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MachineController : Controller
    {
        private readonly ChocolateService _chocolateService;
        private readonly CoffeeService _coffeeService;
        private readonly LemonTeaService _lemonTeaService;

        public MachineController(
            ChocolateService chocolateService,
            CoffeeService coffeeService,
            LemonTeaService lemonTeaService)
        {
            _chocolateService = chocolateService;
            _coffeeService = coffeeService;
            _lemonTeaService = lemonTeaService;
        }

        [HttpGet]
        [Route("")]
        public IActionResult Index()
        {
            return View("DrinkSelection");
        }

        [HttpGet]
        [Route("chocolate")]
        public IActionResult GetChocolate()
        {
            var chocolate = _chocolateService.MakeDrink();

            var model = new DrinkModel(chocolate);

            return View("DrinkSelection", model);
        }

        [HttpGet]
        [Route("coffee")]
        public IActionResult GetCoffee()
        {
            var coffee = _coffeeService.MakeDrink();

            var model = new DrinkModel(coffee);

            return View("DrinkSelection", model);
        }

        [HttpGet]
        [Route("lemon_tea")]
        public IActionResult GetLemonTea()
        {
            var lemonTea = _lemonTeaService.MakeDrink();

            var model = new DrinkModel(lemonTea);

            return View("DrinkSelection", model);
        }
    }
}