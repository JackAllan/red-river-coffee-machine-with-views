using RedRiver.CoffeeMachine.Processor.Services.ChocolateServices;
using RedRiver.CoffeeMachine.Processor.Services.CoffeeServices;
using RedRiver.CoffeeMachine.Processor.Services.LemonTeaServices;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddMvc();
builder.Services.AddSwaggerGen();

builder.Services.AddTransient<ChocolateService>();
builder.Services.AddTransient<CoffeeService>();
builder.Services.AddTransient<LemonTeaService>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();
