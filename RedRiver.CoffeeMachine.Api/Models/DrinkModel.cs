﻿using RedRiver.CoffeeMachine.Domain;
using RedRiver.CoffeeMachine.Domain.Base;

namespace RedRiver.CoffeeMachine.Api.Models
{
    public class DrinkModel
    {
        public DrinkModel(Drink drink)
        {
            PreparationSteps = drink.PreperationSteps;
        }

        public IList<PreperationStep> PreparationSteps { get; set; }
    }
}
