﻿namespace RedRiver.CoffeeMachine.Domain
{
    public class PreperationStep
    {
        public PreperationStep(int sequenceNumber, string description)
        {
            SequenceNumber = sequenceNumber;
            Description = description;
        }

        public int SequenceNumber { get; }
        public string? Description { get; }
    }
}
