﻿namespace RedRiver.CoffeeMachine.Domain.Base
{
    public class Drink
    {
        public IList<PreperationStep> PreperationSteps { get; set; }

        public Drink()
        {
            PreperationSteps = new List<PreperationStep>();
        }
    }
}
