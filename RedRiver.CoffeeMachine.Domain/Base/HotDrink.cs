﻿namespace RedRiver.CoffeeMachine.Domain.Base
{
    public class HotDrink : Drink
    {
        const string BoilWaterStep = "Boil some water";

        public HotDrink()
        {
            PreperationSteps = InitialHotDrinkPreperationSteps;
        }

        readonly IList<PreperationStep> InitialHotDrinkPreperationSteps = new List<PreperationStep>
        {
            new PreperationStep(0, BoilWaterStep)
        };
    }
}
