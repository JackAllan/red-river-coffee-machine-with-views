﻿using RedRiver.CoffeeMachine.Domain.Base;

namespace RedRiver.CoffeeMachine.Processor.Extensions
{
    public static class DrinkExtensions
    {
        public static int GetInitialIndex(this Drink drink)
            => drink.PreperationSteps.Count;
    }
}
