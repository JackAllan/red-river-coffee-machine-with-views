﻿using RedRiver.CoffeeMachine.Domain;
using RedRiver.CoffeeMachine.Services.Interfaces;

namespace RedRiver.CoffeeMachine.Processor.Services.ChocolateServices
{
    public interface IChocolateServices : IDrinkServices<Chocolate>
    {
    }
}
