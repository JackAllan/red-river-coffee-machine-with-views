﻿using RedRiver.CoffeeMachine.Domain;
using RedRiver.CoffeeMachine.Processor.Constants;
using RedRiver.CoffeeMachine.Processor.Extensions;
using RedRiver.CoffeeMachine.Services.Interfaces;

namespace RedRiver.CoffeeMachine.Processor.Services.ChocolateServices
{
    public class ChocolateService : IDrinkServices<Chocolate>
    {
        public Chocolate MakeDrink()
        {
            var chocolateDrink = new Chocolate();

            AddPreperationSteps(chocolateDrink);

            return chocolateDrink;
        }

        private void AddPreperationSteps(Chocolate drink)
        {
            var initialIndex = drink.GetInitialIndex();

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex, 
                    ChocolatePreperationStepConstants.AddChocolateDescription));

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex + 1, 
                    ChocolatePreperationStepConstants.PourChocolateDescription));
        }
    }
}
