﻿using RedRiver.CoffeeMachine.Domain;
using RedRiver.CoffeeMachine.Processor.Constants;
using RedRiver.CoffeeMachine.Processor.Extensions;
using RedRiver.CoffeeMachine.Services.Interfaces;

namespace RedRiver.CoffeeMachine.Processor.Services.LemonTeaServices
{
    public class LemonTeaService : IDrinkServices<LemonTea>
    {
        public LemonTea MakeDrink()
        {
            var lemonTea = new LemonTea();

            AddPreperationSteps(lemonTea);

            return lemonTea;
        }

        private static void AddPreperationSteps(LemonTea drink)
        {
            var initialIndex = drink.GetInitialIndex();

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex,
                    LemonTeaPreperationStepConstants.SteepWaterDescription));

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex + 1,
                    LemonTeaPreperationStepConstants.PourTeaDescription));

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex + 2,
                    LemonTeaPreperationStepConstants.AddLemonDescription));
        }
    }
}
