﻿using RedRiver.CoffeeMachine.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedRiver.CoffeeMachine.Processor.Services.LemonTeaServices
{
    public interface ILemonTeaServices : IDrinkServices<LemonTeaService>
    {
    }
}
