﻿using RedRiver.CoffeeMachine.Domain;
using RedRiver.CoffeeMachine.Processor.Extensions;
using RedRiver.CoffeeMachine.Services.Constants;
using RedRiver.CoffeeMachine.Services.Interfaces;

namespace RedRiver.CoffeeMachine.Processor.Services.CoffeeServices
{
    public class CoffeeService : IDrinkServices<Coffee>
    {
        public Coffee MakeDrink()
        {
            var coffee = new Coffee();

            AddPreperationSteps(coffee);

            return coffee;
        }

        private static void AddPreperationSteps(Coffee drink)
        {
            var initialIndex = drink.GetInitialIndex();

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex,
                    CoffeePreperationStepConstants.BrewCoffeeGroundsDescription));

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex + 1,
                    CoffeePreperationStepConstants.PourCoffeeDescription));

            drink.PreperationSteps.Add(
                new PreperationStep(
                    initialIndex + 2,
                    CoffeePreperationStepConstants.AddSugarAndMilkDescription));
        }
    }
}
