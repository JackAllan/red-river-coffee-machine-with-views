﻿using RedRiver.CoffeeMachine.Domain;
using RedRiver.CoffeeMachine.Services.Interfaces;

namespace RedRiver.CoffeeMachine.Processor.Services.CoffeeServices
{
    internal interface ICoffeeServices : IDrinkServices<Coffee>
    {
    }
}
