﻿namespace RedRiver.CoffeeMachine.Services.Interfaces
{
    public interface IDrinkServices<T>
    {
        T MakeDrink();
    }
}
