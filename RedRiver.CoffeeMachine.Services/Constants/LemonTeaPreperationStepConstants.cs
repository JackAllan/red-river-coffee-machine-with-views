﻿namespace RedRiver.CoffeeMachine.Processor.Constants
{
    public static class LemonTeaPreperationStepConstants
    {
        public const string SteepWaterDescription = "Steep the water in the tea";
        public const string PourTeaDescription = "Pour tea in the cup";
        public const string AddLemonDescription = "Add lemon";
    }
}
