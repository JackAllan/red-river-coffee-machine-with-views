﻿namespace RedRiver.CoffeeMachine.Processor.Constants
{
    public static class ChocolatePreperationStepConstants
    {
        public const string AddChocolateDescription = "Add drinking chocolate powder to water";
        public const string PourChocolateDescription = "Pour chocolate in the cup";
    }
}
