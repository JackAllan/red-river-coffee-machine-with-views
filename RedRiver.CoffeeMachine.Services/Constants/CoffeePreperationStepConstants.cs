﻿namespace RedRiver.CoffeeMachine.Services.Constants
{
    public static class CoffeePreperationStepConstants
    {
        public const string BrewCoffeeGroundsDescription = "Brew the coffee grounds";
        public const string PourCoffeeDescription = "Pour coffee in the cup";
        public const string AddSugarAndMilkDescription = "Add sugar and milk";
    }
}
